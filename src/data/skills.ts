import {
  faBullseye,
  faPencilAlt,
  faBolt,
  faAssistiveListeningSystems,
  faHandshake,
  faTachometerAlt,
} from "@fortawesome/free-solid-svg-icons";

export default [
  {
    name: {
      fr: "Autonome",
      en: "Autonomous",
    },
    icon: faBullseye,
  },
  {
    name: {
      fr: "Polyvalent",
      en: "Versatile",
    },
    icon: faPencilAlt,
  },
  {
    name: {
      fr: "Esprit créatif",
      en: "Creative mind",
    },
    icon: faBolt,
  },
  {
    name: {
      fr: "Attentif",
      en: "Focused",
    },
    icon: faAssistiveListeningSystems,
  },
  {
    name: {
      fr: "Entreprenant",
      en: "Resourceful",
    },
    icon: faHandshake,
  },
  {
    name: {
      fr: "Enthousiaste",
      en: "Enthusiastic",
    },
    icon: faTachometerAlt,
  },
];
