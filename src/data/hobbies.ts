import { faAngellist } from "@fortawesome/free-brands-svg-icons";
import { faMusic, faPlane } from "@fortawesome/free-solid-svg-icons";

export default [
  {
    name: {
      fr: "Mouvement scout",
      en: "Boy-scout",
    },
    icon: faAngellist,
  },
  {
    name: {
      fr: "Piano",
      en: "Piano",
    },
    icon: faMusic,
  },
  {
    name: {
      fr: "Voyages",
      en: "Abroad travels",
    },
    icon: faPlane,
  },
];
