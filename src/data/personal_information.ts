import {
  faLinkedin,
  faGithub,
  faBitbucket,
} from "@fortawesome/free-brands-svg-icons";
import {
  faPhone,
  faEnvelope,
  faMap,
  faGlobe,
} from "@fortawesome/free-solid-svg-icons";

export default {
  linkedin: {
    icon: faLinkedin,
    href: "https://www.linkedin.com/in/antoine-du-hamel/",
    text: "/in/antoine-du-hamel",
  },
  telephone: {
    text: "+33 6 40 20 15 36",
    icon: faPhone,
    href: "tel:+33640201536",
  },
  email: {
    icon: faEnvelope,
    href: "mailto:duhamelantoine1995@gmail.com",
    text: "duhamelantoine1995@gmail.com",
  },
  address: {
    icon: faMap,
    href:
      "https://www.google.fr/maps/place/La+Merci+Dieu,+86270+La+Roche-Posay/@46.764378,0.834581,17z/data=!3m1!4b1!4m5!3m4!1s0x47fc59a1ff228a7b:0x54bb6a6f82327164!8m2!3d46.764378!4d0.836775",
    text: "5 La Merci-Dieu, 86270 La Roche-Posay, France",
  },
  website: {
    icon: faGlobe,
    href: "https://aduh95.github.io/",
    text: "aduh95.github.io",
  },
  github: {
    icon: faGithub,
    href: "https://www.github.com/aduh95",
    text: "aduh95",
  },
  bitbucket: {
    icon: faBitbucket,
    href: "https://www.bitbucket.com/aduh95",
    text: "aduh95",
  },
};
