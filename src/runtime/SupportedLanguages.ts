export type SupportedLanguage = "en" | "fr";

export default ["en", "fr"] as SupportedLanguage[];
